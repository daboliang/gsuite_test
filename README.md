This Notebook serves as a curated process of learning to use
`googledrive` and `googlesheets4`.

According to the
[vignette](https://googlesheets4.tidyverse.org/articles/articles/drive-and-sheets.html)
it’s better to use both packages to access files, reason being
`googlesheets4` only provides sheet level identified by ID (the long
randomly generated string), while combined with `googledrive` you can
access files by specifying its actual **file name**. Also, `googledrive`
serves as the file management interface to your google drive file
system.

Therefore, these packages needed to be installed follow along. Note, you
need the dev version of `googlesheets4` to use `sheet_create()`,
`sheets_write()`. The CRAN version hasn’t incorporated the functions
yet.

    library(googledrive)

    # remotes::install_github('tidyverse/googlesheets4')
    library(googlesheets4)

    ## 
    ## Attaching package: 'googlesheets4'

    ## The following objects are masked from 'package:googledrive':
    ## 
    ##     request_generate, request_make

Auth
----

There’re multiple ways to set up Authorization. Here is to follow the
**Non-interactive** way in which most of the deployment will happen
independent of needing to prompt for entering tokens.

[This
section](https://gargle.r-lib.org/articles/non-interactive-auth.html) is
the definitive reference. For this I am using
[this](https://gargle.r-lib.org/articles/non-interactive-auth.html#project-level-oauth-cache)
section because it appeals to be very commonly used without the need of
having a service account. Also, I found a [GitHub
Repo](https://github.com/JosiahParry/rsc-gsheets) that is also great for
reference.

With that said, perhaps a more “Enterprise” approach would be to get a
Service Account for continuous integration.

In short, it’s a 2 step set up: 1. for a one-time initial set up, you
will need to run `drive_auth()`, `sheets_auth()` and follow the prompt
to get the token down to be cached in a hidden directory within the
project, i.e. `.secrets/`. 2. Then for the deployed app or Rmd, create a
`.Rprofile` to pick up the token stored in `.secrets/` folder which is
bundled together before uploading to RStudio Connect.

The following only need to be ran once
--------------------------------------

    # this is needed for web-based IDE
    options(gargle_oob_default = TRUE)

    # designate project-specific cache
    options(gargle_oauth_cache = ".secrets")

    # check the value of the option, if you like
    # gargle::gargle_oauth_cache()

    # trigger auth on purpose --> store a token in the specified cache. 
    # We stored 2 tokens here one for google drive and one for google sheets
    drive_auth(cache = ".secrets", email = "steve.liang@latimes.com")
    sheets_auth(cache = ".secrets", email = "steve.liang@latimes.com")


    # see your token files in the cache, if you like
    list.files(".secrets/")

You can run the following, or, preferrably, place it in `.Rprofile`
-------------------------------------------------------------------

    options(
      gargle_oauth_cache = ".secrets",   # pick up token cache from .secrets folder
      gargle_oauth_email = "steve.liang@latimes.com"   # email identifier for token
    )

Now the authorzation should be established. We can start to access
files. As mentioned earlier, although you can access file by id, it’s
more friendly to identify file by its name.

Run the following to see if things are set up properly.

    # list max 5 files typed spreadsheet
    googledrive::drive_find(type = "spreadsheet", n_max = 5)

    ## # A tibble: 4 x 3
    ##   name                          id                              drive_resource  
    ## * <chr>                         <chr>                           <list>          
    ## 1 seo_output                    1juz561Hqi5a4ApSXUN4jvV3tigoUp… <named list [34…
    ## 2 Conferences and Travel 2019-… 1KTjpMfAVnH01LYqDa_NV3Q4CqZhNx… <named list [34…
    ## 3 LAT Entertainment SEO Audit … 1liwk740JOeDs_35YPC4eWS0lM8h54… <named list [31…
    ## 4 headline_test                 1Yy6f2nawjC6psAEaHuSuxM-sDLAmB… <named list [33…

Now you can access your files on googledrive. Usually you will need
`googledrive::drive_get` to establish the connection, and then use
`read_sheet()` to actually read the file into R environment. Think
read\_sheet() as `readr::read_csv()` or `readxl::read_excel()`

    # establish the connection by specifying the file name 
    louisa_sheets <- drive_get("LAT Entertainment SEO Audit January 2020")
    # read in the data 
    seo_input_table <- read_sheet(louisa_sheets)

    ## Reading from 'LAT Entertainment SEO Audit January 2020'

    ## Range "Sheet1"

    seo_input_table

    ## # A tibble: 79 x 8
    ##    Date                URL   `Main headline` `SEO headline` `SEO headline c…
    ##    <dttm>              <chr> <chr>           <chr>                     <dbl>
    ##  1 2020-01-15 00:00:00 http… Watch Rachel B… Marvel meets …               62
    ##  2 2020-01-15 00:00:00 http… Hasty, hectic … Review: ‘Doli…               52
    ##  3 2020-01-15 00:00:00 http… Willie Nelson … Willie Nelson…               50
    ##  4 2020-01-15 00:00:00 http… Skirball found… Skirball Cult…               60
    ##  5 2020-01-15 00:00:00 http… Whitney Housto… 2020 Rock and…               82
    ##  6 2020-01-15 00:00:00 http… Review: Isabel… Isabel Allend…               69
    ##  7 2020-01-15 00:00:00 http… The Grammys’ w… Grammy-nomina…               59
    ##  8 2020-01-15 00:00:00 http… Commentary: Be… Beethoven 202…               65
    ##  9 2020-01-15 00:00:00 http… Bold L.A. indi… L.A. indie la…               44
    ## 10 2020-01-16 00:00:00 http… Review: Neo-we… Review: Remem…               64
    ## # … with 69 more rows, and 3 more variables: `SEO headline keywords` <list>,
    ## #   `SEO headline suggestion` <chr>, Notes <chr>

Run Jane’s spacyr script
------------------------

Now since data is down to R env we can do all the manipulation, modeling
here with regular R/Python scripts. Here I continue the process with
Jane’s script which runs spaCy entity recognition code and spits out a
resulting table with summary.

Note I’ve already set up the condaenv using spacyr’s default

    source('spacyr/headline_seo_table.R')

    ## Loading required package: spacyr

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    res <- headline_seo_table(seo_input_table)

    ## Found 'spacy_condaenv'. spacyr will use this environment

    ## successfully initialized (spaCy Version: 2.2.3, language model: en)

    ## (python options: type = "condaenv", value = "spacy_condaenv")

    ## Joining, by = "doc_id"
    ## Joining, by = "doc_id"

Some clean up of the list typed columns before I can write to
googlesheets.

    res$seo_output_table <- res$seo_output_table %>% mutate_if(purrr::is_list, function(x){toString(unlist(x))})

    ## `mutate_if()` ignored the following grouping variables:
    ## Column `doc_id`

This is the one time creation of the final output file.

    ss <- sheets_create(
        "seo_output",
        sheets = list(table=data.frame(x=0), summary=data.frame(x=0))
    )

Write processed table to googlesheets

    # find the existing sheets by name
    ss <- drive_get('seo_output')

    # update the 2 tabs within seo_output sheet
    sheets_write(res$seo_output_table, ss, sheet = "table")

    ## Writing to 'seo_output'

    ## Writing to sheet "table"

    ## Spreadsheet name: seo_output
    ##                 ID: 1juz561Hqi5a4ApSXUN4jvV3tigoUpfOXv62xU5z202Q
    ##             Locale: en_US
    ##          Time zone: Etc/GMT
    ##        # of sheets: 2
    ## 
    ## (Sheet name): (Nominal extent in rows x columns)
    ##        table: 80 x 35
    ##      summary: 2 x 8

    sheets_write(res$seo_output_summary, ss, sheet = "summary")

    ## Writing to 'seo_output'

    ## Writing to sheet "summary"

    ## Spreadsheet name: seo_output
    ##                 ID: 1juz561Hqi5a4ApSXUN4jvV3tigoUpfOXv62xU5z202Q
    ##             Locale: en_US
    ##          Time zone: Etc/GMT
    ##        # of sheets: 2
    ## 
    ## (Sheet name): (Nominal extent in rows x columns)
    ##        table: 80 x 35
    ##      summary: 2 x 8

    cat("SEO flow finished! Opening up the browser")

    ## SEO flow finished! Opening up the browser

    browseURL('https://docs.google.com/spreadsheets/d/1juz561Hqi5a4ApSXUN4jvV3tigoUpfOXv62xU5z202Q/edit#gid=985900284')
